blifoscope
===========

blifoscope Data Analysis

This is my solution to this problem using XOR mapping.

This solution uses Java and some "common" libraries. Please see implementation details at the end.

It's April 1, 2143. Your job is to save the world.

Well, a little world. Specifically the asteroid X8483-Z-32 that you and Alphonso Bliffageri are
stuck on. You've been stranded there ever since the evil Rejectos hit your spaceship with a
slime torpedo fired from one of their spaceships. Now you and Alphonso are trying to save
your little world from a concerted Rejectos attack.

The main problem you have is detecting the Rejectos spaceships and slime torpedos, because
they're protected with cloaking devices. Alphonso has invented an imaging anti-neutrino
system (which he has modestly named the 'blifoscope') that provides the only information
you have about their location, but it's not very good information. First, the blifoscope only
detects whether there are anti-neutrinos at any particular point on an image, not what their
intensity is. In other words, the data it provides is the equivalent of a black-and-white image.
Second, the data is very noisy even if there are no targets in a particular area, some pixels
will be 'on', and if there is a target, some of its pixels will be 'off'. For example, here's a 20 x
20 sample of raw data from the blifoscope (where each '+' is a pixel that is on):


	   +       + ++ + +
	+     +   +++ ++  ++
	     ++    + ++  ++
	+ + ++  + + + + ++
	           ++ ++ +++
	+       +   + +   +
	   +       +
	 +       ++    +
	 +  +          ++  +
	     + +       +
	+       +    +++
	   +       +  +
	    +
	  +    +  +   +    +
	 +       ++      +
	         +  +  +
	     +     +     +
	     +      +
	 +      +          +
	     ++  +  +    +


Below is a sample image of a slime torpedo:

	    +
	    +
	   +++
	 +++++++
	 ++   ++
	++  +  ++
	++ +++ ++
	++  +  ++
	 ++   ++
	 +++++++
	   +++


On the blifoscope data, we've highlighted the pixels that should be 'on' for a slime torpedo.
You can see that more of the highlighted pixels are 'on' in the highlighted area than in other
areas of the image. You must use this difference to locate the targets in the blifoscope data.
Along with this document you've received three files, all text files using '+' symbols to
represent 'on' pixels and spaces to represent 'off' pixels:

1. TestData.txt: a 100 x 100 swath of raw blifoscope data containing between four
and ten targets.

2. SlimeTorpedo.txt: a perfect image of a slime torpedo.

3. Starship.txt: a perfect image of a Rejectos starship.

Your task is to do the following:

1. Design and write a Java package that can analyze arbitrary-sized blifoscope images,
returning a list of targets found. Each target found should include the target type
found (starship or slime torpedo), the coordinates of the target on the blifoscope data,
and some indication of your confidence in the target detection.

2. Design and write test code that submits the test data to your package and prints the
results returned by your package.


Design notes
============

-- blifoscope anti-neutrino detection system - v1.0 Beta (Author: James Lopez <github.com/bluegod>)

** Table of contents **

- Requirements
- Running the application
- View source and Javadoc
- Brief details about the implementation


** Requirements **

Java 1.6 - jar and java should be in the path.


** Running the application **

java -jar blifoscope.jar

This will run the application with the default values of a 70%
chance for a match and ignore +/- 10 points close to target.

We can tweak the application and change the default settings.
For a list of available options we can run:

java -jar blifoscope.jar -h

We can tweak:

- The minimum percentage we want to show on screen for a match.
100% would be a perfect match, 0% would mean no points matched at all.
(command line option -p)

- The points to be ignored (Surrounding points can possible be part
of the same target and not a new target. We may want to adjust this to
a higher value to avoid false positives (that are duplicates of the target)
or lower it if there is a higher chance of target overlapping each other.
Unfortunately, this va;ie has to be the same for all targets in this version
if we run the application using the command line.

(command line option -o)

- Custom data image file, it defaults to TestData.blf if not specified.
(command line option -p)

For each point, it will show the percentage of probability in respect of a
perfect match, plus the location as X (row in the data file) and Y (column
in the data file).

For a data file containing n rows and n columns:

X
|(0,0)
|
|
|
|
|_____________Y (n,n)


** View source and Javadoc **

jar xf blifoscope.jar

This will extract the source code (src folder) and javadoc (javadoc) folder, along
with the libraries used, output, and the rest of the JAR content.

Source structure follows Maven convention and test are located in src/test/java.


** Brief details about the implementation **

I tried to use as less external resources as possible, to make it a simple Java
application. I have only used the commons lang and CLI libraries for better String
manipulation and a better interface to the command line, plus Junit. These libraries should
be available as part of the JAR file. I haven't used any Logger either to make the package
lighter - However, I would probably have put more emphasis in logging if I had done so.

The way I thought the logic of the anti-neutrinos application was simply using a
XOR between the two states of a data file (as bits) that I translated into booleans.
For instance, if there is a + at (12,13) in a torpedo and there is another + in the
data image at the same (12,13) position, it will increase the points by 1 and a relative
percentage would also be increased when running the application (only the first is
kept in memory).

The space item (either a torpedo or a spaceship) will 'move' as a screen or frame
from (0,0) to (n,n) in order to find possible matches.

When targets are found, locations always refer to the left upper corner.

There are some issues with duplicates - What happens if two targets overlap?
What happens if, as the scan is done point by point, assuming a target is found at (0,0)
(0,1) will have a high chance that the total points are nearly as high as the
first target as this may actually be part of the original target.

To avoid that, I've implemented the method:
GenericSpaceItemScanner.duplicateRemoverAtPoint(points, selection)
where for a given range of points, the 'selection' will be ignored (which is
effectively an area of +/- N points near the first target found). I get the 'top'
targets first and order them making sure those are the best targets found and
processed first in the loop.

Personally, and ignoring other implementations I think this is a difficult
problem to implement in the Java programming language. Implementing this in scripting
languages like Ruby would only occupy 1 tenth of the code, due to an easier
way of handling arrays and collections, and having closures (not available untill Java 8).