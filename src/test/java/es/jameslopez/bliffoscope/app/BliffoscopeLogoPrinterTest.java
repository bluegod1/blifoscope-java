package es.jameslopez.bliffoscope.app;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Class: BliffoscopeLogoPrinterTest
 */
public class BliffoscopeLogoPrinterTest {

    @Test
    public void testPrintLogoToScreen() throws Exception {

        final String[] logoText = {null};

        BliffoscopeLogoPrinter logoPrinter = new BliffoscopeLogoPrinter(){
            @Override
            protected void print(String text){
                 logoText[0] = text;
            }
        };
        logoPrinter.printLogoToScreen();
        assertNotNull(logoText[0]);
        int plusCount = StringUtils.countMatches(logoText[0], "-");
        assertEquals(14,plusCount);
    }
}
