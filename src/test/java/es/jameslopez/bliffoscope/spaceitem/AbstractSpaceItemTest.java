package es.jameslopez.bliffoscope.spaceitem;

import junit.framework.Assert;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import static junit.framework.Assert.assertNotNull;
import static org.junit.Assert.assertEquals;

/**
 * Class: AbstractSpaceItemTest
 */
public class AbstractSpaceItemTest {

    @Test
    public void testCreateNeutrinoBinaryImage() throws Exception {
        AbstractSpaceItem spaceItem = new AbstractSpaceItem() {
            @Override
            public String getFileName() {
                return "/SlimeTorpedo.blf";  //Implemented method
            }
        };

        spaceItem.createNeutrinoBinaryImage("/SlimeTorpedo.blf");
        assertEquals(13,spaceItem.getHeight());
        assertEquals(13,spaceItem.getWidth());

        assertNotNull(spaceItem.getAntiNeutrinoBinaryImage());

        String spaceItemString = spaceItem.toString();
        int plusCount = StringUtils.countMatches(spaceItemString,"+");
        //Expecting 0 at this stage as we haven't run any BlfReader.
        Assert.assertEquals(0,plusCount);


    }
}
