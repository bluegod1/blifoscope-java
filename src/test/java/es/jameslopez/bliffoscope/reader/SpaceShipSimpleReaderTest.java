package es.jameslopez.bliffoscope.reader;

import es.jameslopez.bliffoscope.spaceitem.SpaceShip;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Class: TorpedoSimpleReaderTest
 */
public class SpaceShipSimpleReaderTest {

    @Test
    public void testReadSpaceShipData(){

        SpaceShipSimpleReader spaceShipSimpleReader = new SpaceShipSimpleReader();
        SpaceShip spaceShip = (SpaceShip) spaceShipSimpleReader.getSpaceItemFromFile();
        int plusCount = StringUtils.countMatches(spaceShip.toString(), "+");
        assertEquals(54,plusCount);

        int spaceCount = StringUtils.countMatches(spaceShip.toString(), " ");
        assertEquals(171,spaceCount);
    }


}
