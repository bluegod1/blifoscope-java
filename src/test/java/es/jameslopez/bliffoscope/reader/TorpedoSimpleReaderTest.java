package es.jameslopez.bliffoscope.reader;

import es.jameslopez.bliffoscope.spaceitem.Torpedo;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Class: TorpedoSimpleReaderTest
 */
public class TorpedoSimpleReaderTest {

    @Test
    public void testReadTorpedoData(){

        TorpedoSimpleReader torpedoSimpleReader = new TorpedoSimpleReader();
        Torpedo torpedo = (Torpedo) torpedoSimpleReader.getSpaceItemFromFile();
        int plusCount = StringUtils.countMatches(torpedo.toString(), "+");
        assertEquals(47,plusCount);

        int spaceCount = StringUtils.countMatches(torpedo.toString(), " ");
        assertEquals(122,spaceCount);
    }


}
