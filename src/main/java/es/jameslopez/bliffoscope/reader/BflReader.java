package es.jameslopez.bliffoscope.reader;

import es.jameslopez.bliffoscope.spaceitem.AbstractSpaceItem;

/**
 * Class: BflReader
 */
public interface BflReader {

    /**
     * Gets space item.
     *
     * @return the space item
     */
    AbstractSpaceItem getSpaceItem();
}
