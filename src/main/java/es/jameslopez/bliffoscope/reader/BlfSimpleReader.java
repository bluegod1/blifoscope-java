package es.jameslopez.bliffoscope.reader;

import es.jameslopez.bliffoscope.spaceitem.AbstractSpaceItem;
import es.jameslopez.bliffoscope.spaceitem.Image;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Class: BlfSimpleReader
 *
 * Basic reader for .blf type files
 * It can generate an abstract space item or image after parsing the file.
 */
public class BlfSimpleReader implements BflReader {

    /**
     * Gets space item from file.
     * - It will also generate a boolean (bit like) map of the file (anti-neutrino binary image)
     * boolean true = '+'
     * boolean false = ' '
     *
     * @return the space item from file
     */
    public AbstractSpaceItem getSpaceItemFromFile() {

        AbstractSpaceItem spaceItem = getSpaceItem();

        boolean[][] antiNeutrinoBinaryImage = spaceItem.getAntiNeutrinoBinaryImage();

        try {
            InputStream is = null;
            InputStream in;

            if(spaceItem instanceof Image && ((Image) spaceItem).isLoadFromFile()) {
                try {
                    is = new BufferedInputStream((new FileInputStream(spaceItem.getFileName())));
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            } else {

                //Safe use of getResource as no relative path would be used - but we should take this into account.
                in = this.getClass().getResourceAsStream(spaceItem.getFileName());
                if (in == null) {
                    try {
                        throw new FileNotFoundException("File not found in classpath: " + spaceItem.getFileName());
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                }  else {
                    is = new BufferedInputStream(in);
                }
            }
            if(is!=null) {
                BufferedReader reader = new BufferedReader(new InputStreamReader(is,"utf-8"));

                String line;
                int col;
                int row = 0;
                while((line = reader.readLine()) != null && row < spaceItem.getHeight()) {
                    for(col = 0; col < line.length() && col < spaceItem.getWidth(); col++) {
                        //in further versions we may want to stop hard-coding '+' so it can be configured.
                        antiNeutrinoBinaryImage[row][col] = line.charAt(col) == '+';
                    }
                    row++;
                }
                reader.close();
            }
        } catch(IOException e) {
            e.printStackTrace();
        }
        return spaceItem;
    }

    /**
     * Factory method that returns an abstract space item
     * @return the space item
     */
    @Override
    public AbstractSpaceItem getSpaceItem() {
        return null;
    }

}
