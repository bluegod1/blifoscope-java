package es.jameslopez.bliffoscope.reader;

import es.jameslopez.bliffoscope.spaceitem.AbstractSpaceItem;
import es.jameslopez.bliffoscope.spaceitem.SpaceShip;

/**
 * Class: SpaceShipSimpleReader
 *
 * Reads a spaceship file
 */
public class SpaceShipSimpleReader extends BlfSimpleReader {

    @Override
    public AbstractSpaceItem getSpaceItem() {
        return new SpaceShip();
    }
}
