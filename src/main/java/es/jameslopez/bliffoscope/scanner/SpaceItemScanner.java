package es.jameslopez.bliffoscope.scanner;

import es.jameslopez.bliffoscope.spaceitem.AbstractSpaceItem;
import es.jameslopez.bliffoscope.spaceitem.Image;

import java.util.concurrent.ConcurrentNavigableMap;

/**
 * Class: SpaceItemScanner
 */
public interface SpaceItemScanner {
    /**
     * Find space item.
     *
     * @param spaceItem the space item
     * @param image the image
     */
    void findSpaceItem(AbstractSpaceItem spaceItem, Image image);

    /**
     * Get best matches for the original array.
     *
     * @param originalArray the original array
     */
    void getBestMatches(int[][] originalArray);

    /**
     * Gets accumulation point location.
     *
     * @param point the point
     * @return the accumulation point location
     */
    AccumulationPointLocation getAccumulationPointLocation(int point);

    /**
     * Duplicate remover at point.
     *
     * Tries to find any 'duplicates' surrounding a maximum accumulation point, as the surroundings will also have
     * high values but they will belong to the same target.
     *
     * Implementation details would be available on any class implementing this method.
     *
     * @param sortedMapOfPointsAndLocations the sorted map of points and locations
     * @param selection the selection - Points to ignore surrounding the target.
     */
    void duplicateRemoverAtPoint(ConcurrentNavigableMap<Integer, AccumulationPointLocation> sortedMapOfPointsAndLocations, int selection);
}
