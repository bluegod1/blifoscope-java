package es.jameslopez.bliffoscope.scanner;

import java.util.HashMap;
import java.util.Map;

/**
 * Class: AccumulationPointLocation
 *
 * Represents the location for an accumulation point.
 */
public class AccumulationPointLocation {

    Map<Integer,Integer> locationMap;

    /**
     * Instantiates a new Accumulation point location.
     */
    public AccumulationPointLocation() {
        locationMap = new HashMap<Integer,Integer>();
    }

    /**
     * Gets location map.
     *
     * @return the location map
     */
    public Map<Integer, Integer> getLocationMap() {
        return locationMap;
    }

    /**
     * Add location.
     *
     * @param x the x
     * @param y the y
     */
    public void addLocation(int x, int y){
        locationMap.put(x,y);
    }

}
