package es.jameslopez.bliffoscope.spaceitem;

/**
 * Class: SpaceShip
 *
 * Represents a spaceship
 */
public class SpaceShip extends AbstractSpaceItem {

    private static final String fileName = "/Starship.blf";

    /**
     * Instantiates a new Space ship.
     */
    public SpaceShip() {
        super(fileName);
    }

    @Override
    public String getFileName() {
        return fileName;
    }
}
