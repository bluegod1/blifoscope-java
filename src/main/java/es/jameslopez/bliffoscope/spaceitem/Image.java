package es.jameslopez.bliffoscope.spaceitem;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

/**
 * Class: Image
 *
 * Represents an image (data file). Custom methods for getting the
 * input stream based on a file on the file system or classpath
 * are overridden.
 */
public class Image extends AbstractSpaceItem {

    private String imageFileName;
    private boolean loadFromFile;

    /**
     * Instantiates a new Image.
     *
     * @param imageFileName the image file name
     * @param loadFromFile the load from file
     */
    public Image(String imageFileName, boolean loadFromFile) {
        this.imageFileName = imageFileName;
        this.loadFromFile = loadFromFile;
        super.createNeutrinoBinaryImage(imageFileName);
    }

    @Override
    public String getFileName() {
        return imageFileName;
    }

    /**
     * Custom input stream based on how the file is loaded.
     * (classpath / filesystem)
     * @param fileName the file name
     * @return input stream
     */
    @Override
    protected InputStream getInputStream(String fileName) {
        InputStream is = null;
        InputStream in;

        if(loadFromFile) {
            try {
                is = new BufferedInputStream((new FileInputStream(fileName)));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        } else {
            in = this.getClass().getResourceAsStream(fileName);
            if (in == null) {
                try {
                    throw new FileNotFoundException("File not found in classpath: " + fileName);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }  else {
                is = new BufferedInputStream(in);
            }
        }
        return is;
    }

    /**
     * Is load from file.
     *
     * @return the boolean
     */
    public boolean isLoadFromFile() {
        return loadFromFile;
    }
}
